(** OpenAPI specifications. *)

(** This is not a general library, in particular it does not support
    content-types other than [application/json]. It only supports
    what we actually use. *)

module Schema:
sig
  (** OpenAPI Schema Objects.

      Not exactly the same as JSON schemas. *)

  (** Nullable (i.e. non-ref) schemas. *)
  type kind =
    | Boolean
    | Integer of { minimum: int option; maximum: int option; enum: int list }
    | Number of { minimum: float option; maximum: float option }
    | String of { enum: string list; pattern: string option }
    | Array of t
    | Object of { properties: property list; additional_properties: t option }
    | One_of of t list
    | Any

  and t =
    | Ref of string
    | Other of { title: string option; description: string option; nullable: bool; kind: kind }

  (** Object fields. *)
  and property =
    {
      name: string;
      required: bool;
      schema: t;
    }

  type maker = ?title: string -> ?description: string -> ?nullable: bool -> unit -> t

  val boolean: maker
  val integer: ?minimum: int -> ?maximum: int -> ?enum: int list -> maker
  val number: ?minimum: float -> ?maximum: float -> maker
  val string: ?enum: string list -> ?pattern: string -> maker
  val array: items: t -> maker
  val obj: ?additional_properties: t -> properties: property list -> maker
  val one_of: cases: t list -> maker
  val any: maker
  val reference: string -> t
end

module Response:
sig
  type t =
    {
      code: int option;
      description: string;
      schema: Schema.t;
    }

  val make:
    ?code: int ->
    description: string ->
    Schema.t -> t
end

module Service:
sig
  type t =
    {
      description: string;
      request_body: Schema.t option;
      responses: Response.t list;
    }

  val make:
    description: string ->
    ?request_body: Schema.t ->
    Response.t list -> t
end

module Path:
sig
  type dynamic =
    {
      name: string;
      description: string option;
      schema: Schema.t;
    }

  type item =
    | Static of string
    | Dynamic of dynamic

  val static: string -> item

  val dynamic:
    ?description: string ->
    schema: Schema.t ->
    string -> item

  type t = item list
end

module Endpoint:
sig
  type t =
    {
      path: Path.t;
      get: Service.t option;
      post: Service.t option;
    }

  val make:
    ?get: Service.t ->
    ?post: Service.t ->
    Path.t -> t
end

module Server:
sig
  type t =
    {
      url: string;
      description: string option;
    }

  val make:
    ?description: string ->
    string -> t
end

type t =
  {
    title: string;
    description: string option;
    version: string;
    servers: Server.t list;
    definitions: (string * Schema.t) list;
    endpoints: Endpoint.t list;
  }

val make:
  title: string ->
  ?description: string ->
  version: string ->
  ?servers: Server.t list ->
  ?definitions: (string * Schema.t) list ->
  Endpoint.t list -> t

val to_json: t -> Json.t
