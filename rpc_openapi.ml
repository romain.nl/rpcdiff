(* Convert the API description (JSON) into OpenAPI 3 (JSON as well). *)

(* Check our assumptions for arrays. *)
let check_array_specs ({ min_items; max_items; unique_items; additional_items }: Json_schema.array_specs) =
  assert (min_items <= 0);
  assert (max_items = None);
  assert (unique_items = false);
  assert (additional_items = None)

let convert_enum enum convert_item =
  Opt.map (fun enum -> List.map (fun json -> json |> Json_repr.from_any |> convert_item) enum) enum

let rec convert_element (element: Json_schema.element): Openapi.Schema.t =
  (* Check our assumptions. *)
  assert (element.default = None);
  assert (element.format = None);
  assert (element.id = None);

  let maker: Openapi.Schema.maker =
    match element.kind with

      | Object specs ->
          assert (element.enum = None);
          assert (specs.pattern_properties = []);
          assert (specs.min_properties <= 0);
          assert (specs.max_properties = None);
          assert (specs.schema_dependencies = []);
          assert (specs.property_dependencies = []);
          let properties =
            let map l f = List.map f l in
            map specs.properties @@ fun (name, element, required, unknown) ->
            (* [unknown] is not documented, is it a default value? *)
            assert (unknown = None);
            {
              Openapi.Schema.name;
              required;
              schema = convert_element element;
            }
          in
          Openapi.Schema.obj
            ?additional_properties: (Opt.map convert_element specs.additional_properties)
            ~properties

      | Array (elements, specs) ->
          (* Tuples are not supported by OpenAPI, we have to lose some type information. *)
          assert (element.enum = None);
          check_array_specs specs;
          Openapi.Schema.array ~items: (Openapi.Schema.one_of ~cases: (List.map convert_element elements) ())

      | Monomorphic_array (items, specs) ->
          assert (element.enum = None);
          check_array_specs specs;
          Openapi.Schema.array ~items: (convert_element items)

      | Combine (combinator, elements) ->
          assert (element.enum = None);
          assert (combinator = One_of);
          let null = List.exists (function e -> match e.Json_schema.kind with Null -> true | _ -> false) elements in
          let elements = List.filter (function e -> match e.Json_schema.kind with Null -> false | _ -> true) elements in
          (
            fun ?title ?description ?(nullable = false) () ->
              Openapi.Schema.one_of
                ?title
                ?description
                ~nullable: (nullable || null)
                ~cases: (List.map convert_element elements)
                ()
          )

      | Def_ref path ->
          assert (element.enum = None);
          let name =
            match path with
              | `Field "definitions" :: tail ->
                  let path = Json_query.json_pointer_of_path tail in
                  assert (String.length path > 0 && path.[0] = '/');
                  String.sub path 1 (String.length path - 1)
              | _ ->
                  assert false
          in
          (
            fun ?title ?description ?(nullable = false) () ->
              match title, description, nullable with
                | None, None, false ->
                    Openapi.Schema.reference name
                | _ ->
                    (* OpenAPI does not allow other fields next to "$ref" fields.
                       So we have to cheat a little bit. *)
                    Openapi.Schema.one_of ?title ?description ~nullable ~cases: [ Openapi.Schema.reference name ] ()
          )

      | Id_ref _id ->
          assert (element.enum = None);
          assert false

      | Ext_ref _uri ->
          assert (element.enum = None);
          assert false

      | String { pattern; min_length; max_length } ->
          assert (min_length <= 0);
          assert (max_length = None);
          let enum = convert_enum element.enum @@ function `String s -> s | _ -> assert false in
          Openapi.Schema.string ?enum ?pattern

      | Integer { multiple_of; minimum; maximum } ->
          assert (multiple_of = None);
          let enum = convert_enum element.enum @@ function `Int s -> s | _ -> assert false in
          (* Note: there is currently a bug in Json_schema: `Exclusive and `Inclusive are inverted... *)
          let minimum = Opt.map (function (f, `Exclusive) -> int_of_float (ceil f) | _ -> assert false) minimum in
          let maximum = Opt.map (function (f, `Exclusive) -> int_of_float (floor f) | _ -> assert false) maximum in
          Openapi.Schema.integer ?enum ?minimum ?maximum

      | Number { multiple_of; minimum; maximum } ->
          assert (element.enum = None);
          assert (multiple_of = None);
          (* Note: there is currently a bug in Json_schema: `Exclusive and `Inclusive are inverted... *)
          let minimum = Opt.map (function (f, `Exclusive) -> f | _ -> assert false) minimum in
          let maximum = Opt.map (function (f, `Exclusive) -> f | _ -> assert false) maximum in
          Openapi.Schema.number ?minimum ?maximum

      | Boolean ->
          assert (element.enum = None);
          Openapi.Schema.boolean

      | Null ->
          assert (element.enum = None);
          assert false

      | Any ->
          assert (element.enum = None);
          Openapi.Schema.any

      | Dummy ->
          assert (element.enum = None);
          assert false

  in
  maker ?title: element.title ?description: element.description ()

module String_map = Map.Make (String)
type env = Openapi.Schema.t String_map.t
let empty_env = String_map.empty

let merge_envs (a: env) (b: env): env =
  let merge_key _name a b =
    match a, b with
      | None, None ->
          None
      | None, (Some _ as x)
      | (Some _ as x), None ->
          x
      | Some a, Some _b ->
          (* TODO: check that a and b are equivalent *)
          Some a
  in
  String_map.merge merge_key a b

let merge_env_list l = List.fold_left merge_envs empty_env l

let gather_definitions (schema: Json_schema.schema) (converted_schema: Openapi.Schema.t): env =
  let rec gather (acc: env) (converted_schema: Openapi.Schema.t): env =
    match converted_schema with
      | Ref name ->
          if String_map.mem name acc then
            acc
          else (
            match Json_schema.find_definition name schema with
              | exception Not_found ->
                  assert false
              | element ->
                  let converted = convert_element element in
                  let acc = String_map.add name converted acc in
                  gather acc converted
          )
      | Other x ->
          match x.kind with
            | Boolean | Integer _ | Number _ | String _ | Any ->
                acc
            | Array items ->
                gather acc items
            | Object { properties; additional_properties } ->
                let acc = List.fold_left (fun acc p -> gather acc p.Openapi.Schema.schema) acc properties in
                Opt.fold gather acc additional_properties
            | One_of cases ->
                List.fold_left gather acc cases
  in
  gather String_map.empty converted_schema

let convert_schema (schema: Json.t): env * Openapi.Schema.t =
  let schema =
    (schema :> Json_repr.yojson)
    |> Json_repr.from_yojson
    |> Json_schema.of_json
  in
  let converted_schema = convert_element (Json_schema.root schema) in
  let env = gather_definitions schema converted_schema in
  env, converted_schema

let convert_response ?code (schemas: Api.schemas option): env * Openapi.Response.t list =
  match schemas with
    | None ->
        empty_env, []
    | Some schemas ->
        let env, schema = convert_schema schemas.json_schema in
        env, [ Openapi.Response.make ?code ~description: "" schema ]

let opt_map_with_env f = function
  | None ->
      empty_env, None
  | Some x ->
      let env, y = f x in
      env, Some y

let convert_service (service: Api.service): env * Openapi.Service.t =
  let env_1, request_body = opt_map_with_env (fun x -> convert_schema x.Api.json_schema) service.input in
  let env_2, output = convert_response ~code: 200 service.output in
  let env_3, error = convert_response service.error in
  let responses = List.flatten [ output; error ] in
  let service =
    Openapi.Service.make
      ~description: service.description
      ?request_body
      responses
  in
  let env = merge_env_list [ env_1; env_2; env_3 ] in
  env, service

let convert_path_item (path_item: Api.path_item): Openapi.Path.item =
  match path_item with
    | PI_static name ->
        Openapi.Path.static name
    | PI_dynamic arg ->
        Openapi.Path.dynamic
          ?description: arg.descr
          ~schema: (Openapi.Schema.string ())
          arg.name

let convert_path (path: Api.path): Openapi.Path.t =
  List.map convert_path_item path

let convert_endpoint (endpoint: Api.service Api.endpoint): env * Openapi.Endpoint.t =
  let env_1, get = opt_map_with_env convert_service endpoint.get in
  let env_2, post = opt_map_with_env convert_service endpoint.post in
  let endpoint = Openapi.Endpoint.make ?get ?post (convert_path endpoint.path) in
  let env = merge_envs env_1 env_2 in
  env, endpoint

let convert_api (endpoints: Api.service Api.endpoint list): Openapi.t =
  let envs, endpoints = List.map convert_endpoint endpoints |> List.split in
  Openapi.make
    ~title: "Tezos RPC"
    ~description: "Tezos client RPC API."
    ~version: "5" (* TODO: extract it from input? *)
    (* TODO: servers *)
    ~definitions: (String_map.bindings (merge_env_list envs))
    endpoints

let main () =
  (* Parse command line arguments. *)
  let filename =
    let filename = ref None in
    let spec = [] in
    let usage = "rpc_openapi <API.json>" in
    let anon_fun value =
      match !filename with
        | None ->
            filename := Some value
        | Some _ ->
            raise (Arg.Bad ("don't know what to do with: " ^ value))
    in
    Arg.parse spec anon_fun usage;
    let get_mandatory_argument arg_ref =
      match !arg_ref with
        | None ->
            Arg.usage spec usage;
            exit 1
        | Some value ->
            value
    in
    get_mandatory_argument filename
  in

  (* Parse input file and convert it. *)
  Json.from_file filename
  |> Api.parse_tree
  |> Api.parse_services
  |> Api.flatten
  |> convert_api
  |> Openapi.to_json
  |> Json.output

let () =
  Printexc.record_backtrace true;
  try
    main ()
  with exn ->
    Printexc.print_backtrace stderr;
    prerr_endline (Printexc.to_string exn);
    exit 1
