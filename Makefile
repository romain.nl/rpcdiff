default:
	dune build rpc_diff.exe rpc_openapi.exe
	ln -s -f _build/default/rpc_diff.exe rpc_diff
	ln -s -f _build/default/rpc_openapi.exe rpc_openapi

.PHONY: default
