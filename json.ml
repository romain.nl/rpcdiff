include Yojson.Basic

let rec show ?(indent = "") ?(depth = 1) (json: t) =
  match json with
    | `Null ->
        "null"
    | `Bool x ->
        string_of_bool x
    | `Int x ->
        string_of_int x
    | `Float x ->
        string_of_float x
    | `String x ->
        "\"" ^ String.escaped x ^ "\""
    | `Assoc [] ->
        "{}"
    | `Assoc fields ->
        if depth <= 0 then
          "{ ... }"
        else
          let show_field (name, value) =
            "\"" ^ String.escaped name ^ "\": " ^
            show ~indent: (indent ^ "  ") ~depth: (depth - 1) value
          in
          "{\n  " ^ indent ^ String.concat (",\n" ^ indent ^ "  ") (List.map show_field fields) ^ "\n" ^ indent ^ "}"
    | `List items ->
        if depth <= 0 then
          "[ ... ]"
        else
          let show = show ~indent: (indent ^ "  ") ~depth: (depth - 1) in
          "[\n  " ^ indent ^ String.concat (",\n" ^ indent ^ "  ") (List.map show items) ^ "\n" ^ indent ^ "]"

let output_debug ?depth json =
  prerr_endline (show ?depth json)

let output json =
  pretty_to_channel stdout json;
  print_newline ()

let get field json =
  match json with
    | `Assoc l ->
        (
          match List.filter (fun (name, _) -> name = field) l with
            | [] ->
                None
            | [ _, value ] ->
                Some value
            | _ :: _ :: _ ->
                output_debug json;
                failwith ("found multiple occurrences of: " ^ field)
        )
    | _ ->
        output_debug json;
        failwith "expected an object"

let as_list = function
  | `List s ->
      s
  | json ->
      output_debug json;
      failwith "expected a list"

let as_int = function
  | `Int i ->
      i
  | json ->
      output_debug json;
      failwith "expected an integer"

let as_string = function
  | `String s ->
      s
  | json ->
      output_debug json;
      failwith "expected a string"

let as_variant = function
  | `Assoc [ name, value ] ->
      name, value
  | json ->
      output_debug json;
      failwith "expected an object with a single field"

(* Convert an [`Assoc] into a record.
   Ensure no field is left.
   [make] takes an argument to get field from their names,
   and if at the end it did not consume all fields,
   an exception is raised. *)
let as_record json make =
  match json with
    | `Assoc fields ->
        let fields = ref fields in
        let get (field: string) =
          let rec find previous = function
            | [] ->
                None
            | ((head_name, head_value) as head) :: tail ->
                if head_name = field then
                  (
                    fields := List.rev_append previous tail;
                    Some head_value
                  )
                else
                  find (head :: previous) tail
          in
          find [] !fields
        in
        let result = make get in
        if !fields <> [] then (
          output_debug ~depth: 2 json;
          failwith ("some fields were ignored: " ^ String.concat ", " (List.map fst !fields));
        );
        result
    | _ ->
        output_debug ~depth: 2 json;
        failwith "expected an object"

let rec remove_nodes name (json: t): t =
  match json with
    | `Null | `Bool _ | `Int _ | `Float _ | `String _ ->
        json
    | `Assoc fields ->
        let fields =
          fields
          |> List.filter (fun (n, _) -> n <> name)
          |> List.map (fun (n, x) -> n, remove_nodes name x)
        in
        `Assoc fields
    | `List items ->
        `List (List.map (remove_nodes name) items)
