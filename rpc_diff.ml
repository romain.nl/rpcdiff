module String_map = Map.Make (String)

let output x = Printf.ksprintf print_endline x

(* Some JSON lists are considered unordered, in which case their items have a name.
   For instance, for enums (list of strings) the name is simply the string itself;
   and for lists of objects, the name can be a field of these objects. *)

(* To know whether a list is ordered or not, we have a set of heuristics rules:
   a list of JSON paths. All of these JSON paths are to be considered unordered.
   They are associated with a function returning item names. *)

type json_path_item =
  | Root of string (* base type *)
  | Object_field of string (* string = field name *)
  | Ordered_list_item
  | Unordered_list_item

type json_path = json_path_item list

let show_json_path_item = function
  | Root name -> name
  | Object_field name -> "." ^ name
  | Ordered_list_item -> "[]"
  | Unordered_list_item -> "{}"

let show_json_path path =
  String.concat "" (List.map show_json_path_item path)

exception Name_not_available

(* Used to give names to some list items, like { "kind": "dyn", "num_fields": 1, "size": "Uint30" }.
   Reset this at the start of each list. *)
let name_counter = ref 0

let fresh_name base =
  incr name_counter;
  base ^ "_" ^ string_of_int !name_counter

let unordered_lists =
  (* Contrary to the usual bind operator, this one continues in the [None] case.
     The goal is to find the first case which is [Some]. *)
  let (>>=) o f =
    match o with
      | None ->
          f ()
      | Some x ->
          x
  in
  let get_binary_field_name item =
    (
      match item |> Json.get "name" with
        | Some name ->
            Some ("[name = " ^ Json.as_string name ^ "]")
        | None ->
            None
    ) >>= fun () ->
    (
      match item |> Json.get "description" with
        | None ->
            None
        | Some description ->
            match description |> Json.get "title" with
              | None ->
                  None
              | Some title ->
                  Some ("[title = " ^ Json.as_string title ^ "]")
    ) >>= fun () ->
    (
      match item |> Json.get "kind" with
        | Some kind ->
            Some ("[kind = " ^ fresh_name (Json.as_string kind) ^ "]")
        | None ->
            None
    ) >>= fun () ->
    raise Name_not_available
  in
  let get_encoding_case_name item =
    match item with
      | `List [ `Int index; `String name ] ->
          (* Simple lists like in "michelson.v1.primitives.encoding.cases".
             It seems to correspond to variants without parameters (i.e. enums). *)
          "[ " ^ string_of_int index ^ ", " ^ name ^ " ]"
      | _ ->
          match item |> Json.get "tag", item |> Json.get "name" with
            | Some tag, Some name ->
                (* More complex variants (?), usually with a "fields" field for parameters. *)
                "{ " ^ string_of_int (Json.as_int tag) ^ ", " ^ Json.as_string name ^ " }"
            | _ ->
                raise Name_not_available
  in
  let get_one_of_name item =
    (* "oneOf" nodes most often encode variants, with the tag in properties.<SOMETHING>.enum.[0]
       where <SOMETHING> depends on the variant type. *)
    match item |> Json.get "properties" with
      | None ->
          raise Name_not_available
      | Some properties ->
          let try_with_tag_field tag_field =
            match properties |> Json.get tag_field with
              | None ->
                  None
              | Some kind ->
                  match kind |> Json.get "enum" with
                    | None ->
                        None
                    | Some enum ->
                        match enum with
                          | `List [ `String name ] ->
                              Some ("[" ^ name ^ "]")
                          | _ ->
                              None
          in
          try_with_tag_field "kind" >>= fun () ->
          try_with_tag_field "status" >>= fun () ->
          raise Name_not_available
  in
  [
    "service.input.json_schema.", ".required", Json.as_string;
    "service.output.json_schema.", ".required", Json.as_string;
    "service.input.json_schema.", ".enum", Json.as_string;
    "service.output.json_schema.", ".enum", Json.as_string;
    "service.input.binary_schema.", ".fields", get_binary_field_name;
    "service.output.binary_schema.", ".fields", get_binary_field_name;
    "service.input.binary_schema.", ".encoding.cases", get_encoding_case_name;
    "service.output.binary_schema.", ".encoding.cases", get_encoding_case_name;
    "service.input.json_schema.", ".oneOf", get_one_of_name;
    "service.output.json_schema.", ".oneOf", get_one_of_name;
  ]

(* For [Ordered_list_change], we keep the JSON value before and after
   in order to be able to print it for debugging. *)
type json_diff =
  | Object_change of field_diff list
  | Ordered_list_change of json_path * item_diff list * Json.t * Json.t
  | Unordered_list_change of json_path * field_diff list
  | Other_change of Json.t * Json.t

and field_diff =
  | Added_field of string * Json.t
  | Removed_field of string
  | Modified_field of string * json_diff

and item_diff =
  | Added_item of int * Json.t
  | Removed_item of int
  | Modified_item of int * json_diff

type tree_diff_item =
  | Added of Api.meth * Api.path * Json.t
  | Removed of Api.meth * Api.path
  | Modified of Api.meth * Api.path * json_diff

type tree_diff = tree_diff_item list

let rec diff_json (path: json_path) (a: Json.t) (b: Json.t): json_diff option =
  let diff_unordered_fields (x: (string * Json.t) list) (y: (string * Json.t) list) =
    let make_map list =
      List.fold_left
        (fun acc (name, value) -> String_map.add name value acc)
        String_map.empty
        list
    in
    let merge_fields name a b =
      match a, b with
        | None, None ->
            None
        | Some _, None ->
            Some (Removed_field name)
        | None, Some b ->
            Some (Added_field (name, b))
        | Some a, Some b ->
            match diff_json (path @ [ Object_field name ]) a b with
              | None ->
                  None
              | Some diff ->
                  Some (Modified_field (name, diff))
    in
    String_map.merge merge_fields (make_map x) (make_map y)
    |> String_map.bindings
    |> List.map snd
  in
  match a, b with
    | `Null, `Null -> None
    | `Bool x, `Bool y when x = y -> None
    | `Int x, `Int y when x = y -> None
    | `Float x, `Float y when x = y -> None
    | `String x, `String y when x = y -> None
    | `Assoc x, `Assoc y ->
        (* Assume that object field order does not matter. *)
        (
          match diff_unordered_fields x y with
            | [] ->
                None
            | diff ->
                Some (Object_change diff)
        )
    | `List x, `List y ->
        (* Find out whether the current path is in [unordered_lists]. *)
        let get_item_name =
          let match_unordered_criteria (prefix, suffix, _) =
            let path = show_json_path path in
            let len = String.length path in
            let prefix_len = String.length prefix in
            let suffix_len = String.length suffix in
            if prefix_len > len || suffix_len > len then
              false
            else
              String.sub path 0 prefix_len = prefix &&
              String.sub path (len - suffix_len) suffix_len = suffix
          in
          match List.find match_unordered_criteria unordered_lists with
            | exception Not_found ->
                None
            | _, _, get ->
                Some get
        in
        let handle_as_ordered_list () =
          let rec diff_items acc index x y =
            match x, y with
              | [], [] ->
                  List.rev acc
              | _ :: tail_x, [] ->
                  diff_items (Removed_item index :: acc) (index + 1) tail_x []
              | [], head_y :: tail_y ->
                  diff_items (Added_item (index, head_y) :: acc) (index + 1) [] tail_y
              | head_x :: tail_x, head_y :: tail_y ->
                  let acc =
                    match diff_json (path @ [ Ordered_list_item ]) head_x head_y with
                      | None ->
                          acc
                      | Some diff ->
                          Modified_item (index, diff) :: acc
                  in
                  diff_items acc (index + 1) tail_x tail_y
          in
          (
            match diff_items [] 0 x y with
              | [] ->
                  None
              | diff ->
                  Some (Ordered_list_change (path, diff, a, b))
          )
        in
        (
          match get_item_name with
            | None ->
                handle_as_ordered_list ()
            | Some get_item_name ->
                match
                  name_counter := 0;
                  let x_names = List.map (fun item -> get_item_name item, item) x in
                  name_counter := 0;
                  let y_names = List.map (fun item -> get_item_name item, item) y in
                  x_names, y_names
                with
                  | exception Name_not_available ->
                      handle_as_ordered_list ()
                  | x_names, y_names ->
                      (* Unordered list, use an algorithm similar to the one we use for objects. *)
                      match diff_unordered_fields x_names y_names with
                        | [] ->
                            None
                        | diff ->
                            Some (Unordered_list_change (path, diff))
        )
    | (`Null | `Bool _ | `Int _ | `Float _ | `String _ | `Assoc _ | `List _), _ ->
        Some (Other_change (a, b))

(* When a whole tree is new, convert it to a list of [Added] diff items. *)
let rec add_tree path (tree: Json.t Api.tree): tree_diff =
  match tree with
    | Static static ->
        let add_service meth service =
          match service with
            | None ->
                []
            | Some service ->
                [ Added (meth, path, service) ]
        in
        add_service GET static.get_service @
        add_service POST static.post_service @
        (
          match static.subdirs with
            | None ->
                []
            | Some (Suffixes suffixes) ->
                add_suffixes path suffixes
            | Some (Dynamic_dispatch { arg; tree }) ->
                add_tree (path @ [ PI_dynamic arg ]) tree
        )
    | Dynamic _ ->
        output "TODO: added dynamic %s" (Api.show_path path);
        []

and add_suffixes path (suffixes: Json.t Api.suffix list): tree_diff =
  let add_suffix suffix = add_tree (path @ [ PI_static suffix.Api.name ]) suffix.tree in
  List.map add_suffix suffixes
  |> List.flatten

let remove_tree path tree =
  let convert = function
    | Added (meth, path, _) -> Removed (meth, path)
    | Removed _
    | Modified _ -> assert false
  in
  List.map convert (add_tree path tree)

let rec diff_tree path (a: Json.t Api.tree) (b: Json.t Api.tree): tree_diff =
  match a, b with
    | Static a, Static b ->
        diff_static path a b
    | Static _, Dynamic _ ->
        output "TODO: %s was static, it is now dynamic" (Api.show_path path);
        []
    | Dynamic _, Static _ ->
        output "TODO: %s was dynamic, it is now static" (Api.show_path path);
        []
    | Dynamic a, Dynamic b ->
        match diff_json [ Root "dynamic_tree" ] a b with
          | None ->
              []
          | Some _ ->
              output "TODO: dynamic %s has changed" (Api.show_path path);
              []

and diff_static path (a: Json.t Api.static) (b: Json.t Api.static): tree_diff =
  let check_method meth a b =
    match a, b with
      | None, None ->
          []
      | None, Some service ->
          [ Added (meth, path, service) ]
      | Some _, None ->
          [ Removed (meth, path) ]
      | Some a, Some b ->
          match diff_json [ Root "service" ] a b with
            | None ->
                []
            | Some diff ->
                [ Modified (meth, path, diff) ]
  in
  check_method GET a.get_service b.get_service @
  check_method POST a.post_service b.post_service @
  match a.subdirs, b.subdirs with
    | None, None ->
        []
    | None, Some (Suffixes suffixes) ->
        add_suffixes path suffixes
    | None, Some (Dynamic_dispatch { arg; tree }) ->
        add_tree (path @ [ PI_dynamic arg ]) tree
    | Some _, None ->
        output "TODO: %s has been removed" (Api.show_path path);
        []
    | Some (Suffixes _), Some (Dynamic_dispatch _) ->
        output "TODO: %s has been replaced by dynamic dispatch" (Api.show_path path);
        []
    | Some (Dynamic_dispatch _), Some (Suffixes _) ->
        output "TODO: %s has been replaced by static dispatch" (Api.show_path path);
        []
    | Some (Suffixes suffixes_a), Some (Suffixes suffixes_b) ->
        let make_suffix_map list =
          List.fold_left (fun acc s -> String_map.add s.Api.name s acc) String_map.empty list
        in
        let merge_suffixes name (a: Json.t Api.suffix option) (b: Json.t Api.suffix option) =
          let path = path @ [ PI_static name ] in
          match a, b with
            | None, None ->
                None
            | Some a, None ->
                Some (remove_tree path a.tree)
            | None, Some b ->
                Some (add_tree path b.tree)
            | Some a, Some b ->
                Some (diff_tree path a.tree b.tree)
        in
        String_map.merge merge_suffixes (make_suffix_map suffixes_a) (make_suffix_map suffixes_b)
        |> String_map.bindings
        |> List.map snd
        |> List.flatten
    | Some (Dynamic_dispatch { arg = arg_a; tree = tree_a }),
      Some (Dynamic_dispatch { arg = arg_b; tree = tree_b }) ->
        (
          match diff_json [ Root "dynamic_arg" ] arg_a.json arg_b.json with
            | None ->
                ()
            | Some _ ->
                output "TODO: %s argument has changed" (Api.show_path path)
        );
        diff_tree (path @ [ PI_dynamic arg_b ]) tree_a tree_b

let rec flatten_json_diff (json_diff: json_diff): json_diff =
  match json_diff with
    | Object_change field_diffs ->
        Object_change (List.map flatten_field_diff field_diffs)
    | Ordered_list_change (path, item_diffs, a, b) ->
        Ordered_list_change (path, List.map flatten_item_diff item_diffs, a, b)
    | Unordered_list_change (path, field_diffs) ->
        Unordered_list_change (path, List.map flatten_field_diff field_diffs)
    | Other_change _ as x ->
        x

and flatten_item_diff (item_diff: item_diff): item_diff =
  match item_diff with
    | Added_item _ | Removed_item _ as x ->
        x
    | Modified_item (name, json_diff) ->
        Modified_item (name, flatten_json_diff json_diff)

and flatten_field_diff (field_diff: field_diff): field_diff =
  match field_diff with
    | Added_field _ | Removed_field _ as x ->
        x
    | Modified_field (name_1, Object_change [ Modified_field (name_2, sub_diff) ]) ->
        flatten_field_diff (Modified_field (name_1 ^ "." ^ name_2, flatten_json_diff sub_diff))
    | Modified_field (name, json_diff) ->
        Modified_field (name, flatten_json_diff json_diff)

let flatten_diff (diff: tree_diff): tree_diff =
  let flatten_diff_item = function
    | Added _ | Removed _ as x ->
        x
    | Modified (meth, path, json_diff) ->
        Modified (meth, path, flatten_json_diff json_diff)
  in
  List.map flatten_diff_item diff

let rec output_json_diff verbose indent (diff: json_diff) =
  match diff with
    | Object_change field_diff ->
        List.iter (output_field_diff verbose indent) field_diff
    | Ordered_list_change (path, item_diff, a, b) ->
        output "%sOrdered list path: %s" indent (show_json_path path);
        output "Before:";
        Json.output a;
        output "After:";
        Json.output b;
        List.iter (output_item_diff verbose indent) item_diff
    | Unordered_list_change (_, field_diff) ->
        List.iter (output_field_diff verbose indent) field_diff
    | Other_change (a, b) ->
        if verbose then
          (
            output "%sOld value:" indent;
            Json.output a;
            output "%sNew value:" indent;
            Json.output b;
          )

and output_field_diff verbose indent (field_diff: field_diff) =
  match field_diff with
    | Added_field (name, value) ->
        if verbose then
          (
            output "%sAdded %S:" indent name;
            Json.output value;
          )
        else
          output "%sAdded: %S" indent name
    | Removed_field name ->
        output "%sRemoved: %S" indent name
    | Modified_field (name, Other_change (a, b)) ->
        output "%sModified: %S (from %s to %s)" indent name
          (Json.pretty_to_string a) (Json.pretty_to_string b)
    | Modified_field (name, diff) ->
        output "%sModified: %S" indent name;
        output_json_diff verbose (indent ^ "  ") diff

and output_item_diff verbose indent (item_diff: item_diff) =
  match item_diff with
    | Added_item (index, value) ->
        if verbose then
          (
            output "%sAdded item #%i:" indent index;
            Json.output value;
          )
        else
          output "%sAdded item: #%i" indent index
    | Removed_item index ->
        output "%sRemoved item: #%i" indent index
    | Modified_item (index, diff) ->
        output "%sModified item: #%i" indent index;
        output_json_diff verbose (indent ^ "  ") diff

let output_tree_diff_item verbose (diff_item: tree_diff_item) =
  match diff_item with
    | Added (meth, path, service) ->
        output "Added: %s %s" (Api.show_method meth) (Api.show_path path);
        if verbose then Json.output service
    | Removed (meth, path) ->
        output "Removed: %s %s" (Api.show_method meth) (Api.show_path path)
    | Modified (meth, path, diff) ->
        output "Modified: %s %s" (Api.show_method meth) (Api.show_path path);
        output_json_diff verbose "  " diff

let output_tree_diff verbose (diff: tree_diff) =
  List.iter (output_tree_diff_item verbose) diff

let main () =
  (* Parse command line arguments. *)
  let filename_1, filename_2, verbose, mode =
    let before = ref None in
    let after = ref None in
    let verbose = ref false in
    let mode = ref `Both in
    let spec =
      Arg.align [
        "-v", Set verbose, " Verbose mode: output more JSON parts.";
        "-b", Unit (fun () -> mode := `Binary), " Only output binary schemas, not JSON schemas.";
        "-j", Unit (fun () -> mode := `JSON), " Only output JSON schemas, not binary schemas.";
      ]
    in
    let usage = "rpc_diff [OPTIONS] <BEFORE.json> <AFTER.json>" in
    let anon_fun value =
      match !before with
        | None ->
            before := Some value
        | Some _ ->
            match !after with
              | None ->
                  after := Some value
              | Some _ ->
                  raise (Arg.Bad ("don't know what to do with: " ^ value))
    in
    Arg.parse spec anon_fun usage;
    let get_mandatory_argument arg_ref =
      match !arg_ref with
        | None ->
            Arg.usage spec usage;
            exit 1
        | Some value ->
            value
    in
    get_mandatory_argument before, get_mandatory_argument after, !verbose, !mode
  in

  (* Parse input files. *)
  let apply_mode mode json =
    match mode with
      | `Both -> json
      | `Binary -> Json.remove_nodes "json_schema" json
      | `JSON -> Json.remove_nodes "binary_schema" json
  in
  let tree_1 = Json.from_file filename_1 |> apply_mode mode |> Api.parse_tree in
  let tree_2 = Json.from_file filename_2 |> apply_mode mode |> Api.parse_tree in

  (* Compute diff. *)
  let diff = diff_tree [] tree_1 tree_2 in
  let diff = flatten_diff diff in
  output_tree_diff verbose diff

let () =
  Printexc.record_backtrace true;
  try
    main ()
  with exn ->
    Printexc.print_backtrace stderr;
    prerr_endline (Printexc.to_string exn);
    exit 1
