let map f = function
  | None -> None
  | Some x -> Some (f x)

let fold f acc = function
  | None -> acc
  | Some x -> f acc x

let default value = function
  | None -> value
  | Some x -> x

let mandatory name = function
  | None -> failwith ("missing mandatory value: " ^ name)
  | Some x -> x

let to_list = function
  | None -> []
  | Some x -> [ x ]
